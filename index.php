<?php

use src\patterns\Strategy\Lesson\Lesson;
use src\patterns\Strategy\Lesson\Lecture;
use src\patterns\Strategy\Lesson\Seminar;
use src\patterns\Strategy\CostStrategy\FixedCostStrategy;
use src\patterns\Strategy\CostStrategy\TimedCostStrategy;
use src\patterns\Strategy\Notifier\RegistrationMgr;

require 'vendor/autoload.php';

$db = new PDO('mysql:host=localhost;dbname=zandstra;charset=UTF8', 'root',
    'root');
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$lecture = new Lecture(40, new TimedCostStrategy());
$seminar = new Seminar(3, new FixedCostStrategy());

$lessons = [$lecture, $seminar];

foreach ($lessons as $lesson) {
    /* @var $lesson Lesson */
    echo "Плата за занятие: {$lesson->cost()} рублей.\n";
    echo "Тип оплаты: {$lesson->chargeType()}\n\n";
}

$mgr = new RegistrationMgr();
echo $mgr->register($lecture) . "\n";
echo $mgr->register($seminar) . "\n";
