<?php

namespace src\patterns\Strategy\Lesson;

use src\patterns\Strategy\CostStrategy\CostStrategy;

abstract class Lesson
{
    private $duraiton;
    private $costStrategy;

    public function __construct($duraiton, CostStrategy $strategy)
    {
        $this->duraiton = $duraiton;
        $this->costStrategy = $strategy;
    }

    /**
     * @return int
     */
    public function cost(): int
    {
        return $this->costStrategy->cost($this);
    }

    /**
     * @return string
     */
    public function chargeType(): string
    {
        return $this->costStrategy->chargeType();
    }

    /**
     * @return integer
     */
    public function getDuraiton(): int
    {
        return $this->duraiton;
    }
}
