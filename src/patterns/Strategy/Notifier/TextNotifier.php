<?php

namespace src\patterns\Strategy\Notifier;

class TextNotifier
{
    /**
     * @param string $message
     *
     * @return string
     */
    public function inform(string $message): string
    {
        return "Текстовое уведомление: {$message}";
    }
}
