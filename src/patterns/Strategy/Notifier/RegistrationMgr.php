<?php

namespace src\patterns\Strategy\Notifier;

use src\patterns\Strategy\Lesson\Lesson;

class RegistrationMgr
{
    /**
     * @param Lesson $lesson
     *
     * @return string
     * @throws \Exception
     */
    public function register(Lesson $lesson): string
    {
        // Что-то делаем с объектом типа Lesson

        // Рассылка уведомлений
        $notifier = Notifier::getNotifier();
        return $notifier->inform("Новое занятие: стоимость - ({$lesson->cost()}).");
    }
}
