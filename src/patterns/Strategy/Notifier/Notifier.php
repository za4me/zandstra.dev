<?php

namespace src\patterns\Strategy\Notifier;

abstract class Notifier
{
    /**
     * @return MailNotifier|TextNotifier
     * @throws \Exception
     */
    public static function getNotifier(): object
    {
        // Создание класса исходя из какой-то конкретной логики
        if (random_int(1, 2) === 1) {
            return new MailNotifier();
        }

        return new TextNotifier();
    }

    /**
     * @param string $message
     *
     * @return string
     */
    abstract public function inform(string $message): string;
}
