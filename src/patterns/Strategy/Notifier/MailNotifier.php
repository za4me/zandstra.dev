<?php

namespace src\patterns\Strategy\Notifier;

class MailNotifier extends Notifier
{
    /**
     * @param string $message
     *
     * @return string
     */
    public function inform(string $message): string
    {
        return "Уведомление по e-MAIL: {$message}";
    }
}
