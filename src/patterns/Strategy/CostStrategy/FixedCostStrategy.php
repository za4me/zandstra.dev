<?php

namespace src\patterns\Strategy\CostStrategy;

use src\patterns\Strategy\Lesson\Lesson;

class FixedCostStrategy extends CostStrategy
{
    public function cost(Lesson $lesson): int
    {
        return 30;
    }

    public function chargeType(): string
    {
        return 'Фиксированная оплата.';
    }
}
