<?php

namespace src\patterns\Strategy\CostStrategy;

use src\patterns\Strategy\Lesson\Lesson;

class TimedCostStrategy extends CostStrategy
{
    public function cost(Lesson $lesson): int
    {
        return $lesson->getDuraiton() * 5;
    }

    public function chargeType(): string
    {
        return 'Почасовая оплата.';
    }
}
