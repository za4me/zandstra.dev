<?php

namespace src\patterns\Strategy\CostStrategy;

use src\patterns\Strategy\Lesson\Lesson;

abstract class CostStrategy
{
    abstract public function cost(Lesson $lesson): int;

    abstract public function chargeType(): string;
}
