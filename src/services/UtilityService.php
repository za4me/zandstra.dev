<?php

namespace src\services;

use src\traits\PriceUtilities;

class UtilityService extends Service
{
    use PriceUtilities {
        PriceUtilities::calculateTax as private;
    }

    /**
     * @var int
     */
    private $price;

    /**
     * UtilityService constructor.
     * @param integer $price
     */
    public function __construct($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getTaxRate(): int
    {
        return 17;
    }

    /**
     * @return int
     */
    public function getFinalPrice(): int
    {
        return $this->price + $this->calculateTax($this->price);
    }
}
