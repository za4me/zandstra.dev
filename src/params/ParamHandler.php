<?php

namespace src\params;

abstract class ParamHandler
{
    protected $source;
    protected $params = [];

    public function __construct($source)
    {
        $this->source = $source;
    }

    public function addParam($key, $val): void
    {
        $this->params[$key] = $val;
    }

    public function getAllParams(): array
    {
        return $this->params;
    }

    public static function getInstance($filename): object
    {
        if (preg_match('/\.xml$/i', $filename)) {
            return new XmlParamHandler($filename);
        }

        return new TextParamHandler($filename);
    }

    abstract public function write();
    abstract public function read();
}
