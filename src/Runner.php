<?php

namespace src;

class Runner
{
    public static function init()
    {
        try {
            $conf = new Conf('files/conf01.xml');
            echo "{$conf->get('user')}\n";
            echo "{$conf->get('host')}\n";
            $conf->set('pass', 'dummy');
            $conf->write();
        } catch (\Exception $e) {
            exit ($e->getMessage());
        }
    }
}
