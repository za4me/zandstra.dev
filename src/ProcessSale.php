<?php

namespace src;

use Webmozart\Assert\Assert;

class ProcessSale
{
    private $callbacks = [];

    /**
     * @param $callback
     */
    public function registerCallback($callback): void
    {
        Assert::isCallable($callback);
        $this->callbacks[] = $callback;
    }

    /**
     * @param Product $product
     */
    public function sale(Product $product): void
    {
        echo "{$product->getName()}: обрабатывается\n";

        foreach ($this->callbacks as $callback) {
            $callback($product);
        }
    }
}
