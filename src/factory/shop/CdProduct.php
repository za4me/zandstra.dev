<?php

namespace src\factory\shop;

/**
 * Class CdProduct
 * @package src
 */
class CdProduct extends ShopProduct
{
    private $playLength;

    /**
     * CdProduct constructor.
     * @param $title
     * @param $producerMainName
     * @param $producerFirstName
     * @param int $price
     * @param $playLength
     */
    public function __construct(
        $title,
        $producerMainName,
        $producerFirstName,
        $price,
        $playLength
    ) {
        parent::__construct($title, $producerMainName, $producerFirstName,
            $price);
        $this->playLength = $playLength;
    }

    /**
     * @return int
     */
    public function getPlayLength(): int
    {
        return $this->playLength;
    }

    /**
     * @return string
     */
    public function getSummaryLine(): string
    {
        return parent::getSummaryLine() . ": Время звучания - {$this->playLength} мин.";
    }
}
