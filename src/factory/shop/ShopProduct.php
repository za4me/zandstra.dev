<?php

namespace src\factory\shop;

use src\interfaces\Chargeable;
use src\interfaces\IdentityObject;
use src\traits\IdentityTrait;

/**
 * Class ShopProduct
 * @package src
 */
class ShopProduct implements Chargeable, IdentityObject
{
    use IdentityTrait;

    const TYPE_BOOK = 'book';
    const TYPE_CD = 'cd';
    const TYPE_OTHER = 'other';

    private $id;
    private $title;
    private $producerMainName;
    private $producerFirstName;
    protected $price = 0;
    private $discount = 0;

    /**
     * ShopProduct constructor.
     * @param $title
     * @param $producerMainName
     * @param $producerFirstName
     * @param $price
     */
    public function __construct(
        $title,
        $producerMainName,
        $producerFirstName,
        $price
    ) {
        $this->title = $title;
        $this->producerMainName = $producerMainName;
        $this->producerFirstName = $producerFirstName;
        $this->price = $price;
    }

    /**
     * @param $id
     * @param \PDO $pdo
     * @return BookProduct|CdProduct|ShopProduct|null
     */
    public static function getInstance($id, \PDO $pdo): ?self
    {
        $stmt = $pdo->prepare('SELECT * FROM products WHERE id=?');
        $stmt->execute([$id]);

        $row = $stmt->fetch();

        if (empty($row)) {
            return null;
        }

        if ($row['type'] === self::TYPE_BOOK) {
            $product = new BookProduct(
                $row['title'],
                $row['firstname'],
                $row['mainname'],
                $row['price'],
                $row['numpages']
            );
        } elseif ($row['type'] === self::TYPE_CD) {
            $product = new CdProduct(
                $row['title'],
                $row['firstname'],
                $row['mainname'],
                $row['price'],
                $row['playlength']
            );
        } else {
            $product = new self(
                $row['title'],
                $row['firstname'],
                $row['mainname'],
                $row['price']
            );
        }

        $product->setId($row['id']);
        $product->setDiscount($row['discount']);

        return $product;
    }

    /**
     * @return string
     */
    public function getSummaryLine(): string
    {
        return "{$this->title} ({$this->producerMainName}, {$this->producerFirstName})";
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getProducerMainName(): string
    {
        return $this->producerMainName;
    }

    /**
     * @return string
     */
    public function getProducerFirstName(): string
    {
        return $this->producerFirstName;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price - $this->discount;
    }

    /**
     * @return int
     */
    public function getDiscount(): int
    {
        return $this->discount;
    }

    /**
     * @param int $discount
     */
    public function setDiscount(int $discount): void
    {
        $this->discount = $discount;
    }

    /**
     * @return string
     */
    public function getProducer(): string
    {
        return "{$this->producerFirstName} {$this->producerMainName}";
    }
}
