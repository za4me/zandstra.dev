<?php

namespace src\factory\shop;

class BookProduct extends ShopProduct
{
    private $numPages;

    /**
     * BookProduct constructor.
     * @param $title
     * @param $producerMainName
     * @param $producerFirstName
     * @param int $price
     * @param $numPages
     */
    public function __construct(
        $title,
        $producerMainName,
        $producerFirstName,
        $price,
        $numPages
    ) {
        parent::__construct($title, $producerMainName, $producerFirstName,
            $price);
        $this->numPages = $numPages;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getNumPages(): int
    {
        return $this->numPages;
    }

    /**
     * @return string
     */
    public function getSummaryLine(): string
    {
        return parent::getSummaryLine() . ": {$this->numPages} стр.";
    }
}
