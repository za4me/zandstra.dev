<?php

namespace src\factory;

use src\Product;

class Totalizer
{
    /**
     * @param int $amt
     * @return \Closure
     */
    public static function warnAmount(int $amt): \Closure
    {
        $count = 0;
        /**
         * @param Product $product
         */
        return function (Product $product) use ($amt, &$count) {
            $count += $product->getPrice();
            echo "Cумма: {$count}\n";

            if ($count > $amt) {
                echo "\nПродано товаров на сумму: {$count}\n";
            }
        };
    }
}
