<?php

namespace src;

use SimpleXMLElement;
use Webmozart\Assert\Assert;

/**
 * Class Conf
 * @package src
 */
class Conf
{
    /**
     * @var string
     */
    private $file;
    /**
     * @var \SimpleXMLElement
     */
    private $xml;
    /**
     * @var string
     */
    private $lastmatch;

    /**
     * Conf constructor.
     * @param $file
     */
    public function __construct($file)
    {
        $this->file = $file;
        Assert::fileExists($file);

        $this->xml = new SimpleXMLElement($file, null, true);
        Assert::object($this->xml);

        $matches = $this->xml->xpath('/conf');
        Assert::isArray($matches);
    }

    /**
     * Write string to file.
     */
    public function write(): void
    {
        Assert::writable($this->file);
        file_put_contents($this->file, $this->xml->asXML());
    }

    /**
     * @param $str
     * @return null|string
     * @throws \DomainException
     */
    public function get($str): ?string
    {
        $matches = $this->xml->xpath("/conf/item[@name=\"{$str}\"]");
        if (count($matches)) {
            $this->lastmatch = $matches[0];

            return (string)$matches[0];
        }

        throw new \DomainException("Not found {$str} attribute.");
    }

    /**
     * @param $key
     * @param $value
     * @throws \DomainException
     */
    public function set($key, $value): void
    {
        if ($this->get($key) !== null) {
            $this->lastmatch[0] = $value;

            return;
        }

        $this->xml->addChild('item', $value)->addAttribute('name', $key);
    }
}
