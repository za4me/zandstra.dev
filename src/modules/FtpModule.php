<?php

namespace src\modules;

class FtpModule implements Module
{
    public function setHost($host): string
    {
        $class = new \ReflectionClass(self::class);
        $method = $class->getMethod(__FUNCTION__);

        return "{$class->getShortName()}::{$method->getShortName()}: {$host}\n";
    }

    public function setUser($user): string
    {
        $class = new \ReflectionClass(self::class);
        $method = $class->getMethod(__FUNCTION__);

        return "{$class->getShortName()}::{$method->getShortName()}: {$user}\n";
    }

    public function execution()
    {
    }
}
