<?php

namespace src\modules;

interface Module
{
    public function execution();
}
