<?php

namespace src\modules;

class ModuleRunner
{
    private $configData = [
        'PersonModule' => ['person' => 'bob'],
        'FtpModule' => [
            'host' => 'example.com',
            'user' => 'anon',
        ],
    ];

    private $modules = [];

    public function init(): void
    {
        $interface = new \ReflectionClass(Module::class);

        foreach ($this->configData as $moduleName => $params) {
            $moduleClass = new \ReflectionClass("\\src\\modules\\{$moduleName}");
            if ($moduleClass->isSubclassOf($interface) === false) {
                throw new \UnexpectedValueException("Неизвестный тип модуля: {$moduleClass->getName()}\n");
            }

            $module = $moduleClass->newInstance();
            foreach ($moduleClass->getMethods() as $method) {
                $this->handleMethod($module, $method, $params);
            }

            $this->modules[] = $module;
        }
    }

    public function handleMethod(
        Module $module,
        \ReflectionMethod $method,
        $params
    ): bool {
        $name = $method->getName();
        $args = $method->getParameters();

        if (count($args) !== 1 || strpos($name, 'set') === false) {
            return false;
        }

        $property = strtolower(substr($name, 3));

        if (!isset($params[$property])) {
            return false;
        }

        $argClass = $args[0]->getClass();

        if (empty($argClass)) {
            $method->invoke($module, $params[$property]);
        } else {
            $method->invoke($module, $argClass->newInstance($params[$property]));
        }

        return true;
    }
}
