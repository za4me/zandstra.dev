<?php

namespace src\modules;

use src\Person;

class PersonModule implements Module
{
    public function setPerson(Person $person): string
    {
        $class = new \ReflectionClass(self::class);
        $method = $class->getMethod(__FUNCTION__);

        return "{$class->getShortName()}::{$method->getShortName()}: {$person->getName()}\n";
    }

    public function execution()
    {
    }
}
