<?php

namespace src\domains;

abstract class DomainObject
{
    /**
     * @var string
     */
    private $group;

    /**
     * DomainObject constructor.
     */
    public function __construct()
    {
        $this->group = static::getGroup();
    }

    /**
     * @return DomainObject
     */
    public static function create(): self
    {
        return new static();
    }

    /**
     * @return string
     */
    public static function getGroup(): string
    {
        return 'default';
    }
}
