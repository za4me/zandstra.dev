<?php

namespace src\traits;

/**
 * Trait PriceUtilities
 * @package src\traits
 */
trait PriceUtilities
{
    /**
     * @param $price
     * @return int
     */
    protected function calculateTax($price): int
    {
        return ($this->getTaxRate() / 100) * $price;
    }

    /**
     * @return int
     */
    abstract public function getTaxRate(): int;
}
