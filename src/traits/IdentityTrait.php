<?php

namespace src\traits;

/**
 * Trait IdentityTrait
 * @package src\traits
 */
trait IdentityTrait
{
    /**
     * @return int
     */
    public function generateId(): int
    {
        return random_int(1, 1000);
    }
}
