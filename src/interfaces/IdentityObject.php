<?php

namespace src\interfaces;

/**
 * Interface IdentityObject
 * @package src\interfaces
 */
interface IdentityObject
{
    /**
     * @return integer
     */
    public function generateId(): int;
}
