<?php

namespace src\interfaces;

/**
 * Interface Chargeable
 * @package src\interfaces
 */
interface Chargeable
{
    /**
     * @return mixed
     */
    public function getPrice();
}
