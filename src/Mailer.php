<?php

namespace src;

class Mailer
{
    /**
     * @param Product $product
     */
    public function doMail(Product $product)
    {
        echo "Упаковываем ({$product->getName()})\n";
    }
}
