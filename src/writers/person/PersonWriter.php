<?php

namespace src\writers\person;

use src\Person;

/**
 * Class PersonWriter
 * @package src\writers\person
 */
class PersonWriter
{
    /**
     * @param Person $person
     * @return string
     */
    public function writeName(Person $person): string
    {
        return $person->getName();
    }

    /**
     * @param Person $person
     * @return int
     */
    public function writeAge(Person $person): int
    {
        return $person->getAge();
    }
}
