<?php

namespace src\writers\shop;

use src\factory\shop\ShopProduct;

/**
 * Class XmlProductWriter
 * @package src\writers
 */
class XmlProductWriter extends ShopProductWriter
{
    /**
     * @return mixed
     */
    public function write()
    {
        $writer = new \XMLWriter();
        $writer->openMemory();
        $writer->startDocument('1.0', 'UTF-8');
        $writer->startElement('products');

        foreach ($this->products as $product) {
            /* @var $product ShopProduct */
            $writer->startElement('product');
            $writer->writeAttribute('title', $product->getTitle());

            $writer->startElement('summary');
            $writer->text($product->getSummaryLine());
            $writer->endElement(); // summary

            $writer->endElement(); // product
        }

        $writer->endElement(); // products
        $writer->endDocument();

        return $writer->flush();
    }
}
