<?php

namespace src\writers\shop;

use src\factory\shop\ShopProduct;

/**
 * Class TextProductWriter
 * @package src\writers
 */
class TextProductWriter extends ShopProductWriter
{
    /**
     * @return string
     */
    public function write(): string
    {
        $str = "Товары:\n";

        foreach ($this->products as $product) {
            /* @var $product ShopProduct */
            $str .= "{$product->getSummaryLine()}\n";
        }

        return $str;
    }
}
