<?php

namespace src\writers\shop;

use src\factory\shop\ShopProduct;

/**
 * Class ShopProductWriter
 * @package src
 */
abstract class ShopProductWriter
{
    protected $products = [];

    /**
     * @param ShopProduct $shopProduct
     */
    public function addProduct(ShopProduct $shopProduct): void
    {
        $this->products[] = $shopProduct;
    }

    abstract public function write();
}
