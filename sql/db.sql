CREATE TABLE products(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  type TEXT,
  firstname TEXT,
  mainname TEXT,
  title TEXT,
  price FLOAT,
  numpages int,
  playlength int,
  discount int
) CHARACTER SET utf8 COLLATE utf8_unicode_ci;
